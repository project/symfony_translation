/**
 * @file
 * Attaches behaviors for Drupal's active link marking.
 */

(function (Drupal) {
  Drupal.st = async function (id, parameters, domain, locale) {
    domain = domain || 'messages';
    locale = locale || document.documentElement.lang;

    if (!window.symfonyTranslations) {
      window.symfonyTranslations = {};
    }

    if (!symfonyTranslations[domain] || !symfonyTranslations[domain][locale]) {
      const translations = await fetch(`/symfony-translation/translations/${domain}/${locale}`)
        .then(response => response.json());

      if (!symfonyTranslations[domain]) {
        symfonyTranslations[domain] = {};
      }

      symfonyTranslations[domain][locale] = translations;
    }

    let str = id;

    if (symfonyTranslations[domain] && symfonyTranslations[domain][locale] && symfonyTranslations[domain][locale][id]) {
      str = symfonyTranslations[domain][locale][id];
    }

    if (parameters) {
      str = Drupal.formatString(str, parameters);
    }

    return str;
  };
})(Drupal);
