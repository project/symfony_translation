<?php

/**
 * @file
 * Functions and hook implementations for the Symfony Translation module.
 */
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function symfony_translation_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the symfony_translation module.
    case 'help.page.symfony_translation':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Integrates the Symfony Translation component.') . '</p>';
      $output .= '<h3>' . t('More Information') . '</h3>';
      $output .= '<p>' . t('For more information about this module please visit the <a href="@link">module page</a>.', ['@link' => 'https://www.drupal.org/project/symfony_translation']) . '</p>';
      return $output;

    default:
  }
}
/**
 * Implements hook_page_attachments().
 */
function symfony_translation_page_attachments(array &$attachments): void {
  $attachments['#attached']['library'][] = 'symfony_translation/st';
}

/**
 * Implements hook_cache_flush().
 */
function symfony_translation_cache_flush(): void {
  \Drupal::getContainer()
    ->get('symfony_translation.translator')
    ->reset();
}

// phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidPrefix

/**
 * Translates the given message.
 *
 * @param string $id
 *   The message id (may also be an object that can be cast to string)
 * @param array $parameters
 *   An array of parameters for the message.
 * @param string|null $domain
 *   The domain for the message or null to use the default.
 * @param string|null $locale
 *   The locale or null to use the default.
 *
 * @return string
 *   The translated string.
 *
 * @see \Symfony\Contracts\Translation\TranslatorInterface::trans()
 *
 * @throws \InvalidArgumentException
 *   If the locale contains invalid characters.
 */
function st(string $id, array $parameters = [], ?string $domain = NULL, ?string $locale = NULL): string {
  return \Drupal::getContainer()
    ->get('symfony_translation.symfony_translator')
    ->trans($id, $parameters, $domain, $locale);
}
// phpcs:enable
