<?php

namespace Drupal\symfony_translation;

use Drupal\locale\TranslationString;

/**
 * Defines the Symfony Translation string object.
 *
 * This class represents a translation of a source string to a given language,
 * thus it must have at least a 'language' which is the language code and a
 * 'translation' property which is the translated text of the source string
 * in the specified language.
 */
class SymfonyTranslationString extends TranslationString {

  /**
   * {@inheritdoc}
   */
  public function isNew() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isSource() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isTranslation() {
    return TRUE;
  }

}
