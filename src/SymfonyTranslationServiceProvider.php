<?php

namespace Drupal\symfony_translation;

use Drupal\Core\Config\BootstrapConfigStorageFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Sets dynamic container parameters.
 */
class SymfonyTranslationServiceProvider implements ServiceProviderInterface, ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->setParameter('symfony_translation.last_modified', time());
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $config = BootstrapConfigStorageFactory::get()->read('symfony_translation.settings');

    if (isset($config['mode']) && $config['mode'] === 'standalone') {
      $container->getDefinition('symfony_translation.translator')
        ->clearTag('string_translator');
    }
  }

}
