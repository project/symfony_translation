<?php

namespace Drupal\symfony_translation\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\symfony_translation\Translator\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Serves translations for a certain domain and locale.
 */
class TranslationsController implements ContainerInjectionInterface {

  protected Translator $translator;
  protected string $lastModified;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->translator = $container->get('symfony_translation.symfony_translator');
    $instance->lastModified = $container->getParameter('symfony_translation.last_modified');

    return $instance;
  }

  /**
   * Serves translations for a certain domain and locale.
   */
  public function __invoke(Request $request, string $domain, ?string $locale = NULL): JsonResponse {
    $response = new JsonResponse();
    $response->setPublic();
    $response->setLastModified(\DateTimeImmutable::createFromFormat('U', $this->lastModified));

    if ($response->isNotModified($request)) {
      return new JsonResponse(NULL, Response::HTTP_NOT_MODIFIED);
    }

    try {
      $catalogue = $this->translator->getCatalogue($locale);
    }
    catch (\InvalidArgumentException $e) {
      throw new BadRequestHttpException($e->getMessage(), $e);
    }

    while (!in_array($domain, $catalogue->getDomains()) && $fallback = $catalogue->getFallbackCatalogue()) {
      $catalogue = $fallback;
    }

    if (!in_array($domain, $catalogue->getDomains())) {
      throw new BadRequestHttpException(sprintf('Invalid "%s" domain.', $domain));
    }

    return $response->setData($catalogue->all($domain));
  }

}
