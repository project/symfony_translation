<?php

namespace Drupal\symfony_translation\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * A Twig extension for the Symfony translation component.
 */
class TranslatorExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('st', 'st', ['is_safe' => ['html']]),
      new TwigFilter('strans', 'st', ['is_safe' => ['html']]),
    ];
  }

}
