<?php

namespace Drupal\symfony_translation\Form;

use Drupal\Core\DrupalKernel;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\symfony_translation\SymfonyTranslation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for Symfony Translation.
 */
class SettingsForm extends ConfigFormBase {

  protected DrupalKernel $kernel;
  protected SymfonyTranslation $symfonyTranslation;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->kernel = $container->get('kernel');
    $instance->symfonyTranslation = $container->get('symfony_translation.translator');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'symfony_translation_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['symfony_translation.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('symfony_translation.settings');

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Integration mode'),
      '#description' => $this->t('Determines the way the Symfony Translation component will be integrated with Drupal.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('mode'),
      '#options' => [
        'string_translation_provider' => $this->t('String translation provider'),
        'standalone' => $this->t('Standalone'),
      ],
    ];

    $form['mode']['string_translation_provider']['#description'] = sprintf('<ul>%s</ul>', implode('', [
      '<li>' . $this->t('Symfony Translation becomes a string translation provider for Drupal.') . '</li>',
      '<li>' . $this->t('You can translate strings as usual through the t function.') . '</li>',
      '<li>' . $this->t('No advanced features like the ICU MessageFormat syntax.') . '</li>',
      '<li>' . $this->t("It's possible to translate contrib/core strings.") . '</li>',
      '<li>' . $this->t('Translation keys will appear in locale interface and if translated there, will override any file-based translations.') . '</li>',
    ]));

    $form['mode']['standalone']['#description'] = sprintf('<ul>%s</ul>', implode('', [
      '<li>' . $this->t('Symfony Translation becomes a standalone translation system, decoupled from Drupal string translations.') . '</li>',
      '<li>' . $this->t('You can translate strings through the st function.') . '</li>',
      '<li>' . $this->t('You can use the full range of features Symfony Translation offers, like the ICU MessageFormat syntax.') . '</li>',
      '<li>' . $this->t('Can only be used in custom code/templates, unless a contrib module has explicit support.') . '</li>',
      '<li>' . $this->t('No integration with the locale interface (yet).') . '</li>',
    ]));

    $form['folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Translations folder'),
      '#description' => $this->t('The path to the translations folder in this project, relative to the Drupal root.'),
      '#default_value' => $config->get('folder'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('symfony_translation.settings');

    $config
      ->set('mode', $form_state->getValue('mode'))
      ->set('folder', $form_state->getValue('folder'))
      ->save();

    // Trigger a rebuild of the container to make sure the new settings are applied.
    // @see SymfonyTranslationServiceProvider::alter()
    $this->kernel->invalidateContainer();

    // Clear cached message catalogues and translation file paths.
    $this->symfonyTranslation->reset();

    parent::submitForm($form, $form_state);
  }

}
