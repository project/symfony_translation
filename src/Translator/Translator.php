<?php

namespace Drupal\symfony_translation\Translator;

use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\Translation\Translator as TranslatorBase;

/**
 * A translator that gets its locale information from the Drupal language manager.
 */
class Translator extends TranslatorBase {

  protected LanguageManagerInterface $languageManager;

  /**
   * Sets the language manager.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function setLanguageManager(LanguageManagerInterface $languageManager): void {
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocale(): string {
    if (!isset($this->languageManager)) {
      return parent::getLocale();
    }

    return $this->languageManager->getCurrentLanguage()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackLocales(): array {
    if (!isset($this->languageManager)) {
      return parent::getFallbackLocales();
    }

    return $this->languageManager->getFallbackCandidates();
  }

}
