<?php
// phpcs:ignoreFile

/**
 * This file was generated via php core/scripts/generate-proxy-class.php 'Drupal\symfony_translation\Translator\Translator' "web/modules/contrib/symfony_translation/src".
 */

namespace Drupal\symfony_translation\ProxyClass\Translator;

if (\Symfony\Component\HttpKernel\Kernel::VERSION_ID > 50000) {

    /**
     * Provides a proxy class for \Drupal\symfony_translation\Translator\Translator.
     *
     * @see \Drupal\Component\ProxyBuilder
     */
    class Translator implements \Symfony\Contracts\Translation\LocaleAwareInterface, \Symfony\Component\Translation\TranslatorBagInterface, \Symfony\Contracts\Translation\TranslatorInterface
    {

        use \Drupal\Core\DependencyInjection\DependencySerializationTrait;

        /**
         * The id of the original proxied service.
         *
         * @var string
         */
        protected $drupalProxyOriginalServiceId;

        /**
         * The real proxied service, after it was lazy loaded.
         *
         * @var \Drupal\symfony_translation\Translator\Translator
         */
        protected $service;

        /**
         * The service container.
         *
         * @var \Symfony\Component\DependencyInjection\ContainerInterface
         */
        protected $container;

        /**
         * Constructs a ProxyClass Drupal proxy object.
         *
         * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
         *   The container.
         * @param string $drupal_proxy_original_service_id
         *   The service ID of the original service.
         */
        public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, $drupal_proxy_original_service_id)
        {
            $this->container = $container;
            $this->drupalProxyOriginalServiceId = $drupal_proxy_original_service_id;
        }

        /**
         * Lazy loads the real service from the container.
         *
         * @return object
         *   Returns the constructed real service.
         */
        protected function lazyLoadItself()
        {
            if (!isset($this->service)) {
                $this->service = $this->container->get($this->drupalProxyOriginalServiceId);
            }

            return $this->service;
        }

        /**
         * {@inheritdoc}
         */
        public function setLanguageManager(\Drupal\Core\Language\LanguageManagerInterface $languageManager): void
        {
            $this->lazyLoadItself()->setLanguageManager($languageManager);
        }

        /**
         * {@inheritdoc}
         */
        public function getLocale(): string
        {
            return $this->lazyLoadItself()->getLocale();
        }

        /**
         * {@inheritdoc}
         */
        public function getFallbackLocales(): array
        {
            return $this->lazyLoadItself()->getFallbackLocales();
        }

        /**
         * {@inheritdoc}
         */
        public function setConfigCacheFactory(\Symfony\Component\Config\ConfigCacheFactoryInterface $configCacheFactory)
        {
            return $this->lazyLoadItself()->setConfigCacheFactory($configCacheFactory);
        }

        /**
         * {@inheritdoc}
         */
        public function addLoader(string $format, \Symfony\Component\Translation\Loader\LoaderInterface $loader)
        {
            return $this->lazyLoadItself()->addLoader($format, $loader);
        }

        /**
         * {@inheritdoc}
         */
        public function addResource(string $format, mixed $resource, string $locale, ?string $domain = NULL)
        {
            return $this->lazyLoadItself()->addResource($format, $resource, $locale, $domain);
        }

        /**
         * {@inheritdoc}
         */
        public function setLocale(string $locale)
        {
            return $this->lazyLoadItself()->setLocale($locale);
        }

        /**
         * {@inheritdoc}
         */
        public function setFallbackLocales(array $locales)
        {
            return $this->lazyLoadItself()->setFallbackLocales($locales);
        }

        /**
         * {@inheritdoc}
         */
        public function trans(?string $id, array $parameters = array (
        ), ?string $domain = NULL, ?string $locale = NULL): string
        {
            return $this->lazyLoadItself()->trans($id, $parameters, $domain, $locale);
        }

        /**
         * {@inheritdoc}
         */
        public function getCatalogue(?string $locale = NULL): \Symfony\Component\Translation\MessageCatalogueInterface
        {
            return $this->lazyLoadItself()->getCatalogue($locale);
        }

        /**
         * {@inheritdoc}
         */
        public function getCatalogues(): array
        {
            return $this->lazyLoadItself()->getCatalogues();
        }

    }
} else {

    /**
     * Provides a proxy class for \Drupal\symfony_translation\Translator\Translator.
     *
     * @see \Drupal\Component\ProxyBuilder
     */
    class Translator implements \Symfony\Component\Translation\TranslatorBagInterface, \Symfony\Contracts\Translation\TranslatorInterface, \Symfony\Component\Translation\TranslatorInterface
    {

        use \Drupal\Core\DependencyInjection\DependencySerializationTrait;

        /**
         * The id of the original proxied service.
         *
         * @var string
         */
        protected $drupalProxyOriginalServiceId;

        /**
         * The real proxied service, after it was lazy loaded.
         *
         * @var \Drupal\symfony_translation\Translator\Translator
         */
        protected $service;

        /**
         * The service container.
         *
         * @var \Symfony\Component\DependencyInjection\ContainerInterface
         */
        protected $container;

        /**
         * Constructs a ProxyClass Drupal proxy object.
         *
         * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
         *   The container.
         * @param string $drupal_proxy_original_service_id
         *   The service ID of the original service.
         */
        public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, $drupal_proxy_original_service_id)
        {
            $this->container = $container;
            $this->drupalProxyOriginalServiceId = $drupal_proxy_original_service_id;
        }

        /**
         * Lazy loads the real service from the container.
         *
         * @return object
         *   Returns the constructed real service.
         */
        protected function lazyLoadItself()
        {
            if (!isset($this->service)) {
                $this->service = $this->container->get($this->drupalProxyOriginalServiceId);
            }

            return $this->service;
        }

        /**
         * {@inheritdoc}
         */
        public function setLanguageManager(\Drupal\Core\Language\LanguageManagerInterface $languageManager): void
        {
            $this->lazyLoadItself()->setLanguageManager($languageManager);
        }

        /**
         * {@inheritdoc}
         */
        public function getLocale(): string
        {
            return $this->lazyLoadItself()->getLocale();
        }

        /**
         * {@inheritdoc}
         */
        public function getFallbackLocales(): array
        {
            return $this->lazyLoadItself()->getFallbackLocales();
        }

        /**
         * {@inheritdoc}
         */
        public function setConfigCacheFactory(\Symfony\Component\Config\ConfigCacheFactoryInterface $configCacheFactory)
        {
            return $this->lazyLoadItself()->setConfigCacheFactory($configCacheFactory);
        }

        /**
         * {@inheritdoc}
         */
        public function addLoader($format, \Symfony\Component\Translation\Loader\LoaderInterface $loader)
        {
            return $this->lazyLoadItself()->addLoader($format, $loader);
        }

        /**
         * {@inheritdoc}
         */
        public function addResource($format, $resource, $locale, $domain = NULL)
        {
            return $this->lazyLoadItself()->addResource($format, $resource, $locale, $domain);
        }

        /**
         * {@inheritdoc}
         */
        public function setLocale($locale)
        {
            return $this->lazyLoadItself()->setLocale($locale);
        }

        /**
         * {@inheritdoc}
         */
        public function setFallbackLocales(array $locales)
        {
            return $this->lazyLoadItself()->setFallbackLocales($locales);
        }

        /**
         * {@inheritdoc}
         */
        public function trans($id, array $parameters = array(), $domain = NULL, $locale = NULL)
        {
            return $this->lazyLoadItself()->trans($id, $parameters, $domain, $locale);
        }

        /**
         * {@inheritdoc}
         */
        public function transChoice($id, $number, array $parameters = array(), $domain = NULL, $locale = NULL)
        {
            return $this->lazyLoadItself()->transChoice($id, $number, $parameters, $domain, $locale);
        }

        /**
         * {@inheritdoc}
         */
        public function getCatalogue($locale = NULL)
        {
            return $this->lazyLoadItself()->getCatalogue($locale);
        }
    }

}

