<?php

namespace Drupal\symfony_translation;

use Drupal\locale\StringDatabaseStorage;
use Drupal\locale\StringStorageInterface;

/**
 * A database string storage with a fallback to Symfony translations.
 */
class SymfonyTranslationStringStorage implements StringStorageInterface {

  /**
   * The string database storage.
   *
   * @var \Drupal\locale\StringDatabaseStorage
   */
  protected $databaseStorage;

  /**
   * The Symfony translator.
   *
   * @var \Drupal\symfony_translation\SymfonyTranslation
   */
  protected $symfonyTranslator;

  /**
   * Constructs a new SymfonyTranslationStringStorage object.
   *
   * @param \Drupal\locale\StringDatabaseStorage $databaseStorage
   *   The string database storage.
   * @param \Drupal\symfony_translation\SymfonyTranslation $symfonyTranslator
   *   The Symfony translator.
   */
  public function __construct(
    StringDatabaseStorage $databaseStorage,
    SymfonyTranslation $symfonyTranslator
  ) {
    $this->databaseStorage = $databaseStorage;
    $this->symfonyTranslator = $symfonyTranslator;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslations(array $conditions = [], array $options = []) {
    if (isset($conditions['translated']) && $conditions['translated'] === FALSE) {
      return $this->databaseStorage->getTranslations($conditions, $options);
    }

    $translatedStrings = $this->databaseStorage->getTranslations($conditions, $options);
    $untranslatedStrings = $this->databaseStorage->getTranslations(['translated' => FALSE] + $conditions, $options);
    $symfonyStrings = [];

    foreach ($untranslatedStrings as $string) {
      $values = $string->getValues(['lid', 'source', 'context', 'version']);
      $values['translation'] = $this->symfonyTranslator->getStringTranslation($conditions['language'], $values['source'], $values['context']);
      $values['language'] = $conditions['language'];
      $values['storage'] = $this;

      if ($values['translation'] === FALSE) {
        continue;
      }

      $symfonyStrings[$string->lid] = new SymfonyTranslationString($values);
    }

    return array_merge($translatedStrings, $symfonyStrings);
  }

  /**
   * {@inheritdoc}
   */
  public function getStrings(array $conditions = [], array $options = []) {
    return $this->databaseStorage->getStrings($conditions, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function getLocations(array $conditions = []) {
    return $this->databaseStorage->getLocations($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function findString(array $conditions) {
    return $this->databaseStorage->findString($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function findTranslation(array $conditions) {
    return $this->databaseStorage->findTranslation($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function save($string) {
    return $this->databaseStorage->save($string);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($string) {
    return $this->databaseStorage->delete($string);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteStrings($conditions) {
    $this->databaseStorage->deleteStrings($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTranslations($conditions) {
    $this->databaseStorage->deleteTranslations($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function countStrings() {
    return $this->databaseStorage->countStrings();
  }

  /**
   * {@inheritdoc}
   */
  public function countTranslations() {
    return $this->databaseStorage->countTranslations();
  }

  /**
   * {@inheritdoc}
   */
  public function createString($values = []) {
    return $this->databaseStorage->createString($values);
  }

  /**
   * {@inheritdoc}
   */
  public function createTranslation($values = []) {
    return $this->databaseStorage->createTranslation($values);
  }

}
